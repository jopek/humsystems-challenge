# Challenge

## Problem definition

The application should present a small questionnaire for the user to fill out.
Finally, the questionnaire is analysed and the result should be presented to the user.
The communication with the (not existing) server is to be implemented as a mock.

The questionnaire consists of 3 multiple choice questions.
Each question should be queried in a separate step.
Each answer has points.
The points of the chosen answer are added at the end.
The user should be able to jump back to the previous answer.

The questions are:

Do you enjoy working in a team?

* Teamwork is in my blood (5 points)
* Yes, I do (3 points)
* I prefer to work alone (0 points)

How long have you been working with Java?

* Never (0 points)
* Less than 1 year (1 point)
* Less than 2 years (3 points)
* More than 3 years (5 points)

How do you feel about automated tests?

* Mandatory (3 points)
* Waste of time (0 points)

Points evaluation:

* 0-6 points: Unfortunately, we don’t match!
* 7-10 points: That looks good!
* 10 points or more: Excellent!


## Proposed solution
The challenge solution is implemented as Vert.x Telnet Shell Service command `questionnaire`.

### Clone, build, package and run

    $ git clone https://bitbucket.org/jopek/humsystems-challenge
    $ mvn clean package
    $ java -jar target/challenge-1.0-SNAPSHOT.jar

### Connect to questionnaire service

    $ telnet localhost 3000
    Trying ::1...
    telnet: connect to address ::1: Connection refused
    Trying 127.0.0.1...
    Connected to localhost.
    Escape character is '^]'.
    __      __ ______  _____  _______  __   __
    \ \    / /|  ____||  _  \|__   __| \ \ / /
     \ \  / / | |____ | :_) |   | |     \   /
      \ \/ /  |  ____||   __/   | |      > /
       \  /   | |____ | |\ \    | |     / //\
        \/    |______||_| \_\   |_| o  /_/ \_\
    
    
    % questionnaire

### Usage

When executing the shell server's questionnaire command, one is immediately presented the first questionnaire's question.
Each question's answer is enumerated starting with 0, all the way to the *Nth* answer.
The bottom line shows additional navigation keys. 

    Do you enjoy working in a team?
      [0] Teamwork is in my blood
      [1] Yes, I do
      [2] I prefer to work alone
    [p previous] [n next] [e evaluate] [q quit]

Upon question answer (selecting / choosing) a greater-than sign is prepended to the chosen question.
On the last question, choosing *next*, results in re-displaying the last question.
Same thing applies to the first question and choosing the *previous* question.

## Extra points via lambda expressions:

can be found here:
[`ExtraPointsTest.java`](src/test/java/challenge/humsystems/ExtraPointsTest.java)

