package challenge.humsystems.client.model;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class QuestionTest {
    @Test
    public void equality() {
        Question q1 = new Question("is this true?", Arrays.asList("yes", "no"));
        Question q2 = new Question("is this true?", Arrays.asList("yes", "no"));
        Question q3 = new Question("nope", Arrays.asList("yes", "no"));
        Question q4 = new Question("is this true?", Arrays.asList("yes"));

        Question q5 = q1.makeChoice(0);
        Question q6 = q1.makeChoice(0);

        assertEquals(q1, q2);
        assertNotEquals(q1, q3);
        assertNotEquals(q1, q4);

        assertEquals(q1, q5);
        assertEquals(q5, q6);
    }
}
