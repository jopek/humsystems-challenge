package challenge.humsystems.client.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static challenge.humsystems.client.Constants.exampleEvaluationWeights;
import static challenge.humsystems.client.Constants.exampleJsonInput;
import static org.junit.Assert.assertEquals;

public class QuestionnaireTest {

    private Questionnaire questionnaire;

    @Before
    public void setUp() {
        questionnaire = new Questionnaire();
    }

    @Test
    public void back() {
        Question question1 = new Question("schroedingers cat?", Arrays.asList("yes", "maybe", "no"));
        Question question2 = new Question("dadaism", Arrays.asList("yes", "absolutely", "could not agree more"));
        questionnaire.setQuestionList(Arrays.asList(question1, question2));

        assertEquals(question1, questionnaire.nextQuestion());
        assertEquals(question2, questionnaire.nextQuestion());
        assertEquals(question2, questionnaire.nextQuestion());

        assertEquals(question2, questionnaire.previousQuestion());
        assertEquals(question1, questionnaire.previousQuestion());
        assertEquals(question1, questionnaire.previousQuestion());
    }

    @Test
    public void getChoices() {
        Question question1 = new Question("schroedingers cat?", Arrays.asList("yes", "maybe", "no"));
        Question question2 = new Question("dadaism", Arrays.asList("yes", "absolutely", "could not agree more"));
        Question question3 = new Question("how many mm are in one m?", Arrays.asList("2", "100", "300", "1000"));

        questionnaire.setQuestionList(Arrays.asList(question1, question2, question3));

        Question question;

        question = questionnaire.nextQuestion().makeChoice(2);
        questionnaire.updateQuestion(question);
        question = questionnaire.nextQuestion().makeChoice(0);
        questionnaire.updateQuestion(question);
        question = questionnaire.nextQuestion().makeChoice(3);
        questionnaire.updateQuestion(question);

        List<Integer> choiceIndeces = questionnaire.getChoiceIndeces();

        assertEquals(Arrays.asList(2, 0, 3), choiceIndeces);
    }

    @Test
    public void lastReturnedQuestion() {
        Question question1 = new Question("schroedingers cat?", Arrays.asList("yes", "maybe", "no"));
        Question question2 = new Question("dadaism", Arrays.asList("yes", "absolutely", "could not agree more"));
        questionnaire.setQuestionList(Arrays.asList(question1, question2));

        assertEquals(question1, questionnaire.nextQuestion());
        assertEquals(question1, questionnaire.getLastReturnedQuestion());

        assertEquals(question2, questionnaire.nextQuestion());
        assertEquals(question2, questionnaire.getLastReturnedQuestion());
    }

    @Test
    public void evaluation() throws Questionnaire.IncompleteQuestionnaireException {
        questionnaire = Questionnaire.DataMapper.mapToQuestionnaire(exampleJsonInput);

        Question question;
        question = questionnaire.nextQuestion().makeChoice(2);
        questionnaire.updateQuestion(question);
        question = questionnaire.nextQuestion().makeChoice(0);
        questionnaire.updateQuestion(question);
        question = questionnaire.nextQuestion().makeChoice(3);
        questionnaire.updateQuestion(question);

        List<List<Integer>> weights = Questionnaire.DataMapper.mapToWeights(exampleEvaluationWeights);
        String evaluation = questionnaire.evaluation(weights);
        assertEquals("13 points! someone left the gates to heaven ajar...", evaluation);
    }

    @Test(expected = Questionnaire.IncompleteQuestionnaireException.class)
    public void evaluationOfIncomplete() throws Questionnaire.IncompleteQuestionnaireException {
        questionnaire = Questionnaire.DataMapper.mapToQuestionnaire(exampleJsonInput);
        questionnaire.nextQuestion().makeChoice(2);

        List<List<Integer>> weights = Questionnaire.DataMapper.mapToWeights(exampleEvaluationWeights);
        String evaluation = questionnaire.evaluation(weights);
    }


}
