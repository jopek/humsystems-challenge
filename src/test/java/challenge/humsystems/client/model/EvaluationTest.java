package challenge.humsystems.client.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EvaluationTest {

    @Test
    public void evaluationMessage() {
        Evaluation evaluations = new Evaluation();
        evaluations.add(0, 6, "nope");
        evaluations.add(7, 10, "looking good");
        evaluations.add(11, 50, "excellent");
        evaluations.add(61, Integer.MAX_VALUE, "OMG");

        String message;

        message = evaluations.getResultMessage(0);
        assertEquals("nope", message);

        message = evaluations.getResultMessage(0, true);
        assertEquals("0 points! nope", message);

        message = evaluations.getResultMessage(0);
        assertEquals("nope", message);

        message = evaluations.getResultMessage(3);
        assertEquals("nope", message);

        message = evaluations.getResultMessage(8);
        assertEquals("looking good", message);

        message = evaluations.getResultMessage(10);
        assertEquals("looking good", message);

        message = evaluations.getResultMessage(11);
        assertEquals("excellent", message);

        message = evaluations.getResultMessage(123234);
        assertEquals("OMG", message);

        message = evaluations.getResultMessage(123234, true);
        assertEquals("123234 points! OMG", message);

        message = evaluations.getResultMessage(55);
        assertEquals("no message for 55 points set!", message);

    }

}
