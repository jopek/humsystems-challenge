package challenge.humsystems.client;

import challenge.humsystems.client.model.Question;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import static challenge.humsystems.client.Constants.exampleEvaluationWeights;
import static challenge.humsystems.client.Constants.exampleJsonInput;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ControlTest {

    private Control control;
    private Command exitCommand = mock(Command.class);
    private ServiceClient serviceClient = mock(ServiceClient.class);
    private Control.TextOutput textOutput;
    private ArgumentCaptor<Consumer<JsonObject>> questionnaireCaptor = ArgumentCaptor.forClass(Consumer.class);
    private ArgumentCaptor<Consumer<JsonObject>> evaluationCaptor = ArgumentCaptor.forClass(Consumer.class);
    private ArgumentCaptor<Question> questionCaptor = ArgumentCaptor.forClass(Question.class);

    @Before
    public void setUp() {
        textOutput = mock(Control.TextOutput.class);

        control = new Control(serviceClient, textOutput, exitCommand);

        verify(serviceClient).setQuestionsListener(questionnaireCaptor.capture());
        verify(serviceClient).setEvaluationListener(evaluationCaptor.capture());
    }

    @Test
    public void startAndExit() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        control.navigate("q");
        verify(exitCommand).execute();

        verify(textOutput, times(1)).print(questionCaptor.capture());
        verify(textOutput, times(1)).printline(Control.NAVIGATION);

        List<Question> questions = getQuestions();
        assertEquals(questions.get(0), questionCaptor.getValue());
        assertEquals(-1, questionCaptor.getValue().getSelected());
    }

    @Test(expected = IllegalStateException.class)
    public void noQuestionnaireResponseReceived() {
        control.start();
        verify(serviceClient).retrieveQuestions();
        control.navigate("n");
    }

    @Test
    public void startAndExitOnNullInput() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        verify(textOutput, times(1)).print(questionCaptor.capture());
        verify(textOutput, times(1)).printline(Control.NAVIGATION);

        List<Question> questions = getQuestions();
        assertEquals(questions.get(0), questionCaptor.getValue());
        assertEquals(-1, questionCaptor.getValue().getSelected());
    }

    @Test
    public void startWrongInputAndExit() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        Arrays.asList("lala", "q").forEach(control::navigate);
        verify(exitCommand).execute();

        verify(textOutput, times(2)).print(questionCaptor.capture());
        verify(textOutput, times(2)).printline(Control.NAVIGATION);
        verify(textOutput, times(1)).printline(Control.WRONG_INPUT);

        List<Question> questions = getQuestions();
        assertEquals(questions.get(0), questionCaptor.getValue());
        assertEquals(-1, questionCaptor.getValue().getSelected());
    }

    @Test
    public void filloutQuestionnaire() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        Arrays.asList("0", "n", "1", "n", "q").forEach(control::navigate);
        verify(exitCommand).execute();

        verify(textOutput, times(5)).print(questionCaptor.capture());
        verify(textOutput, times(5)).printline(Control.NAVIGATION);

        List<Question> displayedQuestions = questionCaptor.getAllValues();
        List<Question> referenceQuestions = getQuestions();

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(0));
        assertEquals(-1, displayedQuestions.get(0).getSelected());

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(1));
        assertEquals(0, displayedQuestions.get(1).getSelected());

        assertEquals(referenceQuestions.get(1), displayedQuestions.get(2));
        assertEquals(-1, displayedQuestions.get(2).getSelected());
    }

    @Test
    public void filloutBackAndForth() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        Arrays.asList("n", "p", "n", "q").forEach(control::navigate);
        verify(exitCommand).execute();

        verify(textOutput, times(4)).print(questionCaptor.capture());
        verify(textOutput, times(4)).printline(Control.NAVIGATION);

        List<Question> displayedQuestions = questionCaptor.getAllValues();
        List<Question> referenceQuestions = getQuestions();

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(0));
        assertEquals(-1, displayedQuestions.get(0).getSelected());

        assertEquals(referenceQuestions.get(1), displayedQuestions.get(1));
        assertEquals(-1, displayedQuestions.get(1).getSelected());

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(2));
        assertEquals(-1, displayedQuestions.get(2).getSelected());
    }

    @Test
    public void filloutAndEvaluateQuestionnaire() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        Arrays.asList("0", "n", "1", "n", "1", "e", "q").forEach(control::navigate);
        verify(serviceClient).requestEvaluation(anyList());
        verify(exitCommand).execute();

        evaluationCaptor.getValue().accept(exampleEvaluationWeights);

        verify(textOutput, times(7)).print(questionCaptor.capture());
        verify(textOutput, times(7)).printline(Control.NAVIGATION);

        List<Question> displayedQuestions = questionCaptor.getAllValues();
        List<Question> referenceQuestions = getQuestions();

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(0));
        assertEquals(-1, displayedQuestions.get(0).getSelected());

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(1));
        assertEquals(0, displayedQuestions.get(1).getSelected());

        assertEquals(referenceQuestions.get(1), displayedQuestions.get(2));
        assertEquals(-1, displayedQuestions.get(2).getSelected());

        assertEquals(referenceQuestions.get(1), displayedQuestions.get(3));
        assertEquals(1, displayedQuestions.get(3).getSelected());

        assertEquals(referenceQuestions.get(2), displayedQuestions.get(4));
        assertEquals(-1, displayedQuestions.get(4).getSelected());
        assertEquals(1, displayedQuestions.get(5).getSelected());
        assertEquals(1, displayedQuestions.get(6).getSelected());

        verify(textOutput, times(1)).printline("3 points! lappen!");
    }

    @Test
    public void incompleteQuestionnaireEvaluation() {
        control.start();
        verify(serviceClient).retrieveQuestions();

        questionnaireCaptor.getValue().accept(exampleJsonInput);

        Arrays.asList("e", "q").forEach(control::navigate);
        evaluationCaptor.getValue().accept(exampleEvaluationWeights);
        verify(exitCommand).execute();

        verify(textOutput, times(2)).print(questionCaptor.capture());
        verify(textOutput, times(2)).printline(Control.NAVIGATION);
        verify(textOutput, times(1)).printline("not all questions answered!");

        List<Question> displayedQuestions = questionCaptor.getAllValues();
        List<Question> referenceQuestions = getQuestions();

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(0));
        assertEquals(-1, displayedQuestions.get(0).getSelected());

        assertEquals(referenceQuestions.get(0), displayedQuestions.get(1));
        assertEquals(-1, displayedQuestions.get(1).getSelected());
    }


    private List<Question> getQuestions() {
        return Arrays.asList(
                new Question("schroedingers cat?", Arrays.asList("yes", "maybe", "no")),
                new Question("dadaism", Arrays.asList("yes", "absolutely", "could not agree more")),
                new Question("how much is a three?", Arrays.asList("yes", "no", "3", "plants")));
    }
}
