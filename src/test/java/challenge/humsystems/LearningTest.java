package challenge.humsystems;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LearningTest {
    @Test
    public void strListEquality() {
        List<String> s1 = Arrays.asList("a", "b", "c");
        List<String> s2 = Arrays.asList("a", "b", "c");
        List<String> s3 = Arrays.asList("a", "b", "f");
        assertEquals(s1, s2);
        assertNotEquals(s1, s3);
    }

    @Test
    public void getToKnowListIterators() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4);
        ListIterator<Integer> li = integers.listIterator();

        assertEquals(1, li.next().intValue());
        assertEquals(2, li.next().intValue());
        assertNotEquals(1, li.previous().intValue());
        assertEquals(1, li.previous().intValue());
    }

}
