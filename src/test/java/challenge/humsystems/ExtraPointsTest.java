package challenge.humsystems;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ExtraPointsTest {

    // Perform Operation is Odd (): The lambda expression must return true if a number is odd, or false if it is even
    private IntPredicate odd() {
        return i -> i % 2 == 1;
    }

    @Test
    public void oddTest() {
        List<Integer> oddNumbers = IntStream.range(0, 10)
                .filter(odd())
                .boxed()
                .collect(toList());

        assertEquals(Arrays.asList(1, 3, 5, 7, 9), oddNumbers);
    }


    // Perform Operation is Prime (): The lambda expression must return true if a number is prime, or false if it is composite
    //        for (int i = 2; i <= num / 2; i++) {
    //            if (num % i == 0) {
    //                return false;
    //            }
    //        }
    //        return true;
    private IntPredicate prime() {
        return num -> IntStream.range(2, num / 2 + 1).noneMatch(n -> num % n == 0);
    }

    @Test
    public void primeTest() {
        List<Integer> primeNumbers = IntStream.range(0, 20)
                .filter(prime())
                .boxed()
                .collect(toList());

        assertEquals(Arrays.asList(0, 1, 2, 3, 5, 7, 11, 13, 17, 19), primeNumbers);
    }


    // Perform Operation is Palindrome (): The lambda expression must return true if a number is a palindrome, or false if it is not
    private IntPredicate palindrome() {
        return number -> {
            int remainder, sum = 0, prev;

            prev = number;
            while (number > 0) {
                remainder = number % 10;
                sum = (sum * 10) + remainder;
                number = number / 10;
            }
            return prev == sum;
        };
    }

    @Test
    public void palindromeTest() {
        assertTrue(palindrome().test(0));
        assertTrue(palindrome().test(1));
        assertTrue(palindrome().test(2));
        assertTrue(palindrome().test(9));
        assertTrue(palindrome().test(11));
        assertTrue(palindrome().test(22));
        assertTrue(palindrome().test(33));
        assertTrue(palindrome().test(101));
        assertTrue(palindrome().test(111));
        assertTrue(palindrome().test(191));
        assertTrue(palindrome().test(91919));
        assertTrue(palindrome().test(10001));
        assertTrue(palindrome().test(98789));

        assertFalse(palindrome().test(10));
        assertFalse(palindrome().test(12));
        assertFalse(palindrome().test(123));
        assertFalse(palindrome().test(987));
    }
}
