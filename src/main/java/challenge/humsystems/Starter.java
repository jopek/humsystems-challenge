package challenge.humsystems;

import challenge.humsystems.client.ClientVerticle;
import challenge.humsystems.server.ServerVerticle;
import io.vertx.core.Vertx;

public class Starter {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new ServerVerticle(), event -> System.out.println(event.result()));
        vertx.deployVerticle(new ClientVerticle(), event -> System.out.println(event.result()));
    }
}
