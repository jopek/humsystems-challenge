package challenge.humsystems.client.model;

import java.util.List;
import java.util.Objects;

public class Question {
    private final String question;
    private final List<String> choices;
    private final int selected;

    public Question(String question, List<String> choices) {
        this.question = question;
        this.choices = choices;
        this.selected = -1;
    }

    public Question(String question, List<String> choices, int choice) {
        this.question = question;
        this.choices = choices;
        this.selected = choice;
    }

    public Question makeChoice(int choicesIndex){
        return new Question(question, choices, choicesIndex);
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getChoices() {
        return choices;
    }

    public int getSelected() {
        return selected;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", choices=" + choices +
                ", selected=" + selected +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question1 = (Question) o;
        return getQuestion().equals(question1.getQuestion()) &&
                getChoices().equals(question1.getChoices());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuestion(), getChoices());
    }
}
