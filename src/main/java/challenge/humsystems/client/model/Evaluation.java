package challenge.humsystems.client.model;

import org.apache.commons.lang3.Range;

import java.util.HashMap;
import java.util.Map;

public class Evaluation {
    private Map<Range<Integer>, String> evaluations = new HashMap<>();

    public void add(int lowerBound, int upperBound, String message) {
        evaluations.put(Range.between(lowerBound, upperBound), message);
    }

    public String getResultMessage(int points) {
        return getResultMessage(points, false);
    }

    public String getResultMessage(int points, boolean withPointsInMessage) {
        String message = evaluations.entrySet()
                .stream()
                .filter(e -> e.getKey().contains(points))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(String.format("no message for %d points set!", points));

        String pts = String.format("%d points! ", points);
        return String.format("%s%s", withPointsInMessage ? pts : "", message);
    }

}
