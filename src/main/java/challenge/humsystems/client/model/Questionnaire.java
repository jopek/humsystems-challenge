package challenge.humsystems.client.model;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Questionnaire {

    private List<Question> questionList = Collections.emptyList();
    private ListIterator<Question> questionListIterator;
    private Evaluation evaluation = new Evaluation();
    private Question lastReturnedQuestion = null;

    public void setQuestionList(List<Question> questions) {
        this.questionList = questions;
        questionListIterator = questions.listIterator();
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public Question nextQuestion() {
        if (questionListIterator.hasNext())
            lastReturnedQuestion = questionListIterator.next();
        return lastReturnedQuestion;
    }

    public void updateQuestion(Question updated) {
        questionListIterator.set(updated);
        lastReturnedQuestion = updated;
    }

    public Question previousQuestion() {
        if (questionListIterator.hasPrevious())
            lastReturnedQuestion = questionListIterator.previous();
        return lastReturnedQuestion;
    }

    public Question getLastReturnedQuestion() {
        return lastReturnedQuestion;
    }

    public List<Integer> getChoiceIndeces() {
        return questionList.stream()
                .map(Question::getSelected)
                .collect(toList());
    }

    public String evaluation(List<List<Integer>> weights) throws IncompleteQuestionnaireException {
        int points = evaluationPoints(weights);
        return evaluationMessage(points);
    }

    private String evaluationMessage(int score) {
        return evaluation.getResultMessage(score, true);
    }

    private int evaluationPoints(List<List<Integer>> weights) throws IncompleteQuestionnaireException {
        List<Integer> choiceIndeces = getChoiceIndeces();

        if (choiceIndeces.size() != weights.size())
            throw new IllegalArgumentException("number of answered questions not the same as in evaluation");

        if (choiceIndeces.stream().anyMatch(c -> c == -1))
            throw new IncompleteQuestionnaireException();

        return IntStream.range(0, choiceIndeces.size())
                .map(index -> weights.get(index).get(choiceIndeces.get(index)))
                .sum();
    }


    public static class DataMapper {
        public static Questionnaire mapToQuestionnaire(JsonObject entries) {
            Questionnaire questionnaire = new Questionnaire();
            questionnaire.setQuestionList(mapToQuestionList(entries));
            questionnaire.setEvaluation(mapToEvaluation(entries));
            return questionnaire;
        }

        public static List<Question> mapToQuestionList(JsonObject questions) {
            return questions.getJsonArray("questions", new JsonArray())
                    .stream()
                    .map(questionJson -> (JsonObject) questionJson)
                    .map(DataMapper::mapToQuestion)
                    .collect(toList());
        }

        private static Question mapToQuestion(JsonObject questionJson) {
            List<String> answers = questionJson.getJsonArray("answers")
                    .stream()
                    .map(a -> (String) a)
                    .collect(toList());
            String question = questionJson.getString("question");
            return new Question(question, answers);
        }

        public static Evaluation mapToEvaluation(JsonObject evaluationJsonObject) {
            Evaluation evaluation = new Evaluation();

            evaluationJsonObject.getJsonArray("evaluation", new JsonArray())
                    .stream()
                    .map(jsonObject -> (JsonObject) jsonObject)
                    .forEach(e -> {
                        JsonArray bounds = e.getJsonArray("bounds", new JsonArray());
                        int lower = bounds.getInteger(0);
                        int upper = Integer.MAX_VALUE;
                        if (bounds.size() == 2)
                            upper = bounds.getInteger(1);
                        evaluation.add(lower, upper, e.getString("message"));
                    });

            return evaluation;
        }

        public static List<List<Integer>> mapToWeights(JsonObject evaluationJsonObject) {
            // [question0[idx0, idx1, idx2], ..., questionN[idx0, ..., idxN]]

            return evaluationJsonObject.getJsonArray("weights", new JsonArray())
                    .stream()
                    .map(e -> (JsonArray) e)
                    .map(objects -> objects.stream()
                            .map(e -> (Integer) e)
                            .collect(toList()))
                    .collect(toList());

        }
    }

    public class IncompleteQuestionnaireException extends Throwable {
        public IncompleteQuestionnaireException() {
            super("not all questions answered!");
        }
    }
}
