package challenge.humsystems.client;

import challenge.humsystems.client.model.Question;
import challenge.humsystems.client.model.Questionnaire;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Objects.isNull;

public class Control {
    public static final String NAVIGATION = "[p previous] [n next] [e evaluate] [q quit]";
    public static final String WRONG_INPUT = "# please enter one of <NUMBER>, e, n, p, q";

    private enum Direction {FORWARD, BACKWARD}

    private Direction previousDirection = Direction.FORWARD;

    private final ServiceClient serviceClient;
    private final TextOutput textOutput;
    private final Command exitCommand;
    private Questionnaire questionnaire;
    private List<List<Integer>> weights;

    public Control(
            ServiceClient serviceClient,
            TextOutput textOutput,
            Command exitCommand) {
        this.serviceClient = serviceClient;
        this.textOutput = textOutput;
        this.exitCommand = exitCommand;

        serviceClient.setQuestionsListener(jsonObject -> {
            questionnaire = Questionnaire.DataMapper.mapToQuestionnaire(jsonObject);
            navigate("n");
            textOutput.flush();
        });

        serviceClient.setEvaluationListener(jsonObject -> {
            weights = Questionnaire.DataMapper.mapToWeights(jsonObject);
            evaluate();
            textOutput.flush();
        });
    }

    public void start() {
        serviceClient.retrieveQuestions();
    }

    public void navigate(String input) {
        if (isNull(questionnaire))
            throw new IllegalStateException("questionnaire not yet initialized");

        if (isNull(input) || input.isEmpty())
            return;

        Question question = questionnaire.getLastReturnedQuestion();

        int choice;

        switch (input.toLowerCase()) {
            case "n":
                if (previousDirection == Direction.BACKWARD)
                    questionnaire.nextQuestion();
                question = questionnaire.nextQuestion();
                previousDirection = Direction.FORWARD;
                break;

            case "p":
                if (previousDirection == Direction.FORWARD)
                    questionnaire.previousQuestion();
                question = questionnaire.previousQuestion();
                previousDirection = Direction.BACKWARD;
                break;

            case "e":
                List<Integer> choices = questionnaire.getChoiceIndeces();
                serviceClient.requestEvaluation(choices);
                break;

            case "q":
                exitCommand.execute();
                return;

            default:
                try {
                    choice = Integer.parseUnsignedInt(input);
                    question = question.makeChoice(choice);
                    questionnaire.updateQuestion(question);
                } catch (NumberFormatException ignored) {
                    textOutput.printline(WRONG_INPUT);
                }
        }

        textOutput.printline("");
        textOutput.print(question);
        textOutput.printline(NAVIGATION);
    }

    private void evaluate() {
        try {
            String evaluation = questionnaire.evaluation(weights);
            textOutput.printline(evaluation);
        } catch (Questionnaire.IncompleteQuestionnaireException e) {
            textOutput.printline(e.getMessage());
        }
    }

    // https://stackoverflow.com/questions/6415728/junit-testing-with-simulated-user-input
    public static class TextOutput {
        private final PrintStream out;
        private final Command flushCommand;

        public TextOutput(PrintStream out, Command flushCommand) {
            this.out = out;
            this.flushCommand = flushCommand;
        }

        public void flush() {
            flushCommand.execute();
        }

        public void printline(String line) {
            out.println(line);
        }

        public void print(Question question) {
            printline(question.getQuestion());
            List<String> choices = question.getChoices();
            IntStream.range(0, choices.size()).forEach(idx -> {
                String formattedLine = String.format("%s [%d] %s", question.getSelected() == idx ? ">" : " ", idx, choices.get(idx));
                out.println(formattedLine);
            });
        }
    }


}
