package challenge.humsystems.client;

public interface Command {
    void execute();
}
