package challenge.humsystems.client;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.ext.shell.ShellService;
import io.vertx.ext.shell.ShellServiceOptions;
import io.vertx.ext.shell.command.Command;
import io.vertx.ext.shell.command.CommandBuilder;
import io.vertx.ext.shell.command.CommandRegistry;
import io.vertx.ext.shell.term.TelnetTermOptions;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClientVerticle extends AbstractVerticle {

    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(ClientVerticle.class.getName());
    }

    @Override
    public void start() {

        ServiceClient serviceClient = new ServiceClient(vertx);

        Command questionnaireCommand = CommandBuilder.command("questionnaire").
                processHandler(process -> {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    PrintStream ps = new PrintStream(os);
                    AtomicBoolean exitRequested = new AtomicBoolean(false);

                    Control.TextOutput output = new Control.TextOutput(ps, () -> process.write(os.toString()));
                    Control control = new Control(serviceClient, output, () -> {
                        process.end();
                        exitRequested.set(true);
                    });
                    control.start();

                    process.stdinHandler(remoteEnteredKeys -> {
                        control.navigate(remoteEnteredKeys);
                        String textOutput = os.toString();
                        System.out.println(textOutput);

                        if (exitRequested.get())
                            return;

                        process.write(textOutput);
                    });

                    // Terminate when user hits Ctrl-C
                    process.interruptHandler(v -> {
                        process.end();
                        exitRequested.set(true);
                    });

                }).build(vertx);

        ShellService service = ShellService.create(vertx, new ShellServiceOptions().setTelnetOptions(
                new TelnetTermOptions().setHost("localhost").setPort(3000)
        ));

        CommandRegistry.getShared(vertx).registerCommand(questionnaireCommand);

        service.start(ar -> {
            if (!ar.succeeded()) {
                ar.cause().printStackTrace();
            }
        });
    }
}
