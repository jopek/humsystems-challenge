package challenge.humsystems.client;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.function.Consumer;

public class ServiceClient {

    private final EventBus eventBus;
    private Consumer<JsonObject> questionsConsumer;
    private Consumer<JsonObject> evaluationConsumer;

    public ServiceClient(Vertx vertx) {
        eventBus = vertx.eventBus();
    }

    public void retrieveQuestions() {
        eventBus.<JsonObject>request("questions", new JsonObject(), event -> {
            System.out.println("questions request reply");
            if (event.failed()) {
                System.out.println("failed... :(");
                return;
            }

            if (questionsConsumer != null) {
                Message<JsonObject> result = event.result();
                questionsConsumer.accept(result.body());
            }
        });
    }

    public void setQuestionsListener(Consumer<JsonObject> consumer) {
        this.questionsConsumer = consumer;
    }

    public void requestEvaluation(List<Integer> choices) {
        eventBus.<JsonObject>request("evaluation", new JsonArray(choices), event -> {
            System.out.println("evaluation request reply");
            if (event.failed()) {
                System.out.println("failed... :(");
                return;
            }

            if (evaluationConsumer != null) {
                Message<JsonObject> result = event.result();
                evaluationConsumer.accept(result.body());
            }
        });
    }

    public void setEvaluationListener(Consumer<JsonObject> consumer) {
        this.evaluationConsumer = consumer;
    }
}
