package challenge.humsystems.server;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ServerVerticle extends AbstractVerticle {

    @Override
    public void start() {
        vertx.eventBus()
                .consumer("questions", event -> {
                    System.out.println(event.address());
                    event.reply(questionsResponse);
                });
        vertx.eventBus()
                .<JsonArray>consumer("evaluation", event -> {
                    System.out.println(event.address());
                    System.out.println(event.body().encodePrettily());
                    event.reply(evaluationWeightsResponse);
                });
    }


    public static JsonObject questionsResponse = new JsonObject()
            .put("questions", new JsonArray()
                    .add(new JsonObject()
                            .put("question", "Do you enjoy working in a team?")
                            .put("answers", new JsonArray()
                                    .add("Teamwork is in my blood ")
                                    .add("Yes, I do")
                                    .add("I prefer to work alone")
                            )
                    )
                    .add(new JsonObject()
                            .put("question", "How long have you been working with Java?")
                            .put("answers", new JsonArray()
                                    .add("Never")
                                    .add("Less than 1 year")
                                    .add("Less than 2 years")
                                    .add("More than 3 years")
                            )
                    )
                    .add(new JsonObject()
                            .put("question", "How do you feel about automated tests?")
                            .put("answers", new JsonArray()
                                    .add("Mandatory")
                                    .add("Waste of time")
                            )
                    )
            )
            .put("evaluation", new JsonArray()
                    .add(new JsonObject()
                            .put("bounds", new JsonArray("[0,6]"))
                            .put("message", "Unfortunately, we don’t match!")
                    )
                    .add(new JsonObject()
                            .put("bounds", new JsonArray("[7,9]"))
                            .put("message", "That looks good!")
                    )
                    .add(new JsonObject()
                            .put("bounds", new JsonArray("[10]"))
                            .put("message", "Excellent!")
                    )
            );

    public static JsonObject evaluationWeightsResponse = new JsonObject()
            .put("weights", new JsonArray()
                    .add(new JsonArray()
                            .add(5)
                            .add(3)
                            .add(0)
                    )
                    .add(new JsonArray()
                            .add(0)
                            .add(1)
                            .add(3)
                            .add(5)
                    )
                    .add(new JsonArray()
                            .add(3)
                            .add(0)
                    )
            );

}
